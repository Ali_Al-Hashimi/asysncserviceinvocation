package org.camunda.asysncinvocation;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Asynchronous1 App")
public class AsysncInvocationApp1 extends ServletProcessApplication {
  // empty implementation

}