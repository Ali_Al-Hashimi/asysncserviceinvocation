package org.camunda.quickstart.servicetask.invocation;


import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.quickstart.servicetask.invocation.MockMessageQueue.Message;




/**
 * This class represents the actual business logic invoked by the service task.
 * 
 */
public class BusinessLogic {
  
  //public static final String SHOULD_FAIL_VAR_NAME = "shouldFail";
	
	
	   // putting the text in variables
  public static final String Msg = "timer completed with seconds =";
  public static final int Times = 5;
 
  
  public static BusinessLogic INSTANCE = new BusinessLogic();
    
  public void invoke(Message message, ProcessEngine processEngine) throws InterruptedException {    
	  
	   
	  TimeUnit.SECONDS.sleep(5);
    // Process the message and send a callback.
    
    // Extract values from payload:     
    Map<String, Object> requestPayload = message.getPayload();
    // the execution id is used as correlation identifier 
    String executionId = (String) requestPayload.get(AsynchronousServiceTask.EXECUTION_ID);    

    // putting the   variables contained the text in Callback Payload
     Map<String, Object> callbackPayload = Collections.<String,Object>singletonMap(Msg, Times);
      processEngine.getRuntimeService().signal(executionId, callbackPayload);
  
   // }
  }

}