package org.camunda.quickstart.servicetask.invocation.sync;

import static org.junit.Assert.*;

import org.junit.Test;




import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.quickstart.servicetask.invocation.AsynchronousServiceTask;
import org.camunda.quickstart.servicetask.invocation.BusinessLogic;
import org.camunda.quickstart.servicetask.invocation.MockMessageQueue;
import org.camunda.quickstart.servicetask.invocation.MockMessageQueue.Message;
import org.junit.Rule;

/**
 * Test case for demonstrating the asynchronous service invocation.
 * 
 */
public class TestAsynchronousServiceTask {

  @Rule
  public ProcessEngineRule processEngineRule = new ProcessEngineRule();

  @Test
  @Deployment(resources = { "lastSEQ.bpmn" })
  public void testServiceInvocationSuccessful() throws InterruptedException {

    final ProcessEngine processEngine = processEngineRule.getProcessEngine();
    final RuntimeService runtimeService = processEngineRule.getRuntimeService();
    final TaskService taskService = processEngineRule.getTaskService();

    // this invocation should NOT fail
    Map<String, Object> variables = Collections.<String, Object> singletonMap(BusinessLogic.Msg, false);

    // start the process instance
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("asynInvocc1", variables);

    // the process instance is now waiting in the first wait state (user task):
    Task userTask_1 = taskService.createTaskQuery()
      .taskDefinitionKey("userTask_1")
      .processInstanceId(processInstance.getId())
      .singleResult();
    assertNotNull(userTask_1);

    // Complete the first task. This triggers causes the Service task to be executed. 
    // After the method call returns, the message will be put into the queue and 
    // the process instance is waiting in the service task activity.
    taskService.complete(userTask_1.getId());

    // the process instance is now waiting in the service task activity:
    assertEquals(Arrays.asList("AsyncServiceTask"), runtimeService.getActiveActivityIds(processInstance.getId()));
    
    // the message is present in the Queue:
    Message message = MockMessageQueue.INSTANCE.getNextMessage();
    assertNotNull(message);
    assertEquals(processInstance.getId(), message.getPayload().get(AsynchronousServiceTask.EXECUTION_ID));

    // Next, trigger the business logic. This will send the callback to the process engine.
    // When this method call returns, the process instance will be waiting in the next waitstate.
    BusinessLogic.INSTANCE.invoke(message, processEngine);
    
    // the process instance is now waiting in the second wait state (user task):
    Task userTask_2 = taskService.createTaskQuery()
      .taskDefinitionKey("userTask_2")
      .processInstanceId(processInstance.getId())
      .singleResult();
    assertNotNull(userTask_2);
        
    // check for variable set by the service task:
    variables = runtimeService.getVariables(processInstance.getId());
    assertEquals(BusinessLogic.Times, variables.get(BusinessLogic.Msg));

  }

  @Test
  @Deployment(resources = { "lastSEQ.bpmn" })
  public void testServiceInvocationFailure() {

    final ProcessEngine processEngine = processEngineRule.getProcessEngine();
    final RuntimeService runtimeService = processEngine.getRuntimeService();
    final TaskService taskService = processEngine.getTaskService();

    // this invocation should fail
    Map<String, Object> variables = Collections.<String, Object> singletonMap(BusinessLogic.Msg, true);

    // start the process instance
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("asynInvocc1", variables);

    // the process instance is now waiting in the first wait state (user task):
    Task userTask_1 = taskService.createTaskQuery()
      .taskDefinitionKey("userTask_1")
      .processInstanceId(processInstance.getId())
      .singleResult();
    assertNotNull(userTask_1);

    // Complete the first task. This triggers causes the Service task to be executed. 
    // After the method call returns, the message will be put into the queue and 
    // the process instance is waiting in the service task activity.
    taskService.complete(userTask_1.getId());

    // the process instance is now waiting in the service task activity:
    assertEquals(Arrays.asList("AsyncServiceTask"), runtimeService.getActiveActivityIds(processInstance.getId()));
    
    // the message is present in the Queue:
    Message message = MockMessageQueue.INSTANCE.getNextMessage();
    assertNotNull(message);
    assertEquals(processInstance.getId(), message.getPayload().get(AsynchronousServiceTask.EXECUTION_ID));


    // the process instance is still waiting in the service task activity:
    assertEquals(Arrays.asList("AsyncServiceTask"), runtimeService.getActiveActivityIds(processInstance.getId()));

  }

}